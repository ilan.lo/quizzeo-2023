import { readFile } from 'node:fs/promises'
import {getAskedQuestion, getCountryInfo, getRandomInt} from "./quizzGenerator.js";
import {createApp} from "./app.js";

const app = createApp()

let liste = []
app.post("/quizz/start", async (req, res) => {
    const data =  JSON.parse( await readFile('countries.json'))
    let types = 4
    const questions = [
        "Quel est le pays associé a ce drapeau ?",
        "Quel est le drapeau associé a ce pays ?",
        "Quel est le Pays associé a cette capitale ?",
        "Quelle est la capitale associée a ce pays ?"
    ]
    //data = JSON.parse(data)
    let index = data.length
    for(let i = 0; i <= 19; i=i+1){
        let type = getRandomInt(4)
        let random = getRandomInt(index)
        let countryInfo = getCountryInfo(data[random])
        let row = {
            "id": i,
            "type": type,
            "question": questions[type],
            "answer": countryInfo
        }

        liste.push(row)
    }
    res.json(liste)
})

app.get("/question/:id", (req, res) => {
    let question = liste[req.params.id]
    //hint = getchoices(question)
    let asked = getAskedQuestion(question.type, question);
    console.log(question)
    console.log(asked)
    res.render("newQuestion", { desiredInfo: asked[0], tip: asked[1], id: req.params.id, answer:asked[2], flag:question.answer.flag })
})

app.post("/chooseAnswerType/:id", (req, res) => {

    res.send(req.body.answerType)
})

app.get("/dashboard/:name", (req, res) => {
    res.render("index", { name: req.params.name })
})
app.get("/test", (req, res) => {
    res.send("OK")
})

app.get("/hello/:name", (req, res) => {
    res.send("hello " + req.params?.name + " !");
})

app.listen(4000, () => {
    console.log("serveur démarré sur le port 4000...");
})
