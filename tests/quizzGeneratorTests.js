import { expect } from "chai";

import {createQuestion, createQuizz, getOtherChoices, getRandomInt, getAskedQuestion} from "../quizzGenerator.js";
import fs from 'fs';

describe('quizzGenerator Tests', function() {

    it('Should create 20 questions from countries data', () => {
        // const countries = [
        //     { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
        //     { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
        //     { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
        //     { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
        //     { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } }
        // ]
        // const quizz = createQuizz(countries)
        expect(true).to.be.true
    });


    it('Should create one question from 4 countries data', () => {
        const countries = [
            { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
            { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
            { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
            { "flags": { "png": "https://flagcdn.com/w320/mh.png", "svg": "https://flagcdn.com/mh.svg", "alt": "The flag of Marshall Islands has a blue field with two broadening adjacent diagonal bands of orange and white that extend from the lower hoist-side corner to the upper fly-side corner of the field. A large white star with twenty-four rays — four large rays at the cardinal points and twenty smaller rays — is situated in the upper hoist-side corner above the diagonal bands." }, "name": { "common": "Marshall Islands", "official": "Republic of the Marshall Islands", "nativeName": { "eng": { "official": "Republic of the Marshall Islands", "common": "Marshall Islands" }, "mah": { "official": "Republic of the Marshall Islands", "common": "M̧ajeļ" } } }, "capital": ["Majuro"] }, { "flags": { "png": "https://flagcdn.com/w320/fi.png", "svg": "https://flagcdn.com/fi.svg", "alt": "The flag of Finland has a white field with a large blue cross that extend to the edges of the field. The vertical part of this cross is offset towards the hoist side." } },
        ]

        const question = createQuestion(countries)

        expect(question.tip).to.be.ok
        expect(question.tipType).to.be.ok
        expect(question.tipType).to.be.oneOf(["capital","country","flag"])
        expect(question.desiredInfo).to.be.ok
        expect(question.choices).to.be.ok
        expect(question.choices).to.be.an("Array")
        expect(question.choices.length).to.eql(4)
    });
    let liste
    it('Should create 20 questions', () => {
        liste = createQuizz()
        const questions = [
            "Quel est le pays associé a ce drapeau ?",
            "Quel est le drapeau associé a ce pays ?",
            "Quel est le Pays associé a cette capitale ?",
            "Quelle est la capitale associée a ce pays ?"
        ]
        expect(liste.length).to.eql(20)
        expect(liste).to.be.an("Array")
        //console.log(liste)
        liste.forEach((e, index) => {
            expect(e.type).to.be.an("number")
            expect(e.type).to.be.lessThan(4)
            expect(e.type).to.be.greaterThan(-1)
            expect(e.id == index).to.be.true
            expect(e.question).to.be.oneOf(questions)
        })
        expect(5).to.be.eql(5)

    })
    it('Should extract information from each questions', () => {
        liste.forEach((elt, index) => {
            let answer = getAskedQuestion(elt.type, elt)
            expect(answer).to.be.an("Array")
            expect(answer[0]).to.be.oneOf(["le Pays","le Drapeau","la capitale"])
            expect(answer[1]).to.be.oneOf(["ce Drapeau","ce Pays","cette Capitale"])
            expect(answer[2]).to.be.oneOf([elt.answer.country, elt.answer.flag, elt.answer.capital])

        })
    });
    it('Should create random int in range', () => {
        for(let i = 1; i <= 10; i++){
            let number = getRandomInt(i)
            expect(number).to.be.an("number")
            expect(number).to.be.lessThan(i)
            expect(number).to.be.greaterThan(-1)
        }
    });
    it('Should create 3 other good choices', () => {
        //TODO
        liste.forEach((elt, index) => {
            let choices = getOtherChoices(elt)
            choices.push(elt.answer)
            //console.log(choices)
            //console.log(elt.answer)
            expect(elt.answer).to.be.oneOf(choices)
            //expect(choices[0].country).to.be.oneOf(choices)
        })

    });

});