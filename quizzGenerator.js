import fs from "fs";

export function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
export const getAskedQuestion = (type, row) => {
    let q = "{\n" +
        "  id: 2,\n" +
        "  type: 3,\n" +
        "  question: 'Quelle est la capitale associée a ce pays ?',\n" +
        "  answer: {\n" +
        "    country: 'French Polynesia',\n" +
        "    flag: 'https://flagcdn.com/w320/pf.png',\n" +
        "    capital: [ 'Papeetē' ]\n" +
        "  }\n" +
        "}"
    let toReturn;

    switch (type){
        case 0:
            toReturn = ["le Pays", "ce Drapeau", row.answer.country]
            break;
        case 1:
            toReturn = ["le Drapeau", "ce Pays", row.answer.flag]
            break;
        case 2:
            toReturn = ["le Pays", "cette Capitale", row.answer.country]
            break;
        case 3:
            toReturn = ["la capitale", "ce Pays", row.answer.capital]
            break;
    }
    //console.log("oui : " + toReturn)
    return toReturn
}
export const getCountryInfo = (data) => {
    let useFull = {
        "country":data.name.official,
        "flag":data.flags.png,
        "capital":data.capital[0]
    }
    return useFull
}
export const data =  JSON.parse( fs.readFileSync('countries.json'))
export const createQuizz = (countries) => {
    let liste = []

    let types = 4
    const questions = [
        "Quel est le pays associé a ce drapeau ?",
        "Quel est le drapeau associé a ce pays ?",
        "Quel est le Pays associé a cette capitale ?",
        "Quelle est la capitale associée a ce pays ?"
    ]
    //data = JSON.parse(data)
    let index = data.length
    for(let i = 0; i <= 19; i=i+1){
        let type = getRandomInt(4)
        let random = getRandomInt(index)
        let countryInfo = getCountryInfo(data[random])
        let row = {
            "id": i,
            "type": type,
            "question": questions[type],
            "answer": countryInfo
        }

        liste.push(row)
    }
    return liste
}
export const getOtherChoices = (choice) => {
    //return a list of 3 choices like the one they give {
    //     id: 19,
    //     type: 3,
    //     question: 'Quelle est la capitale associée a ce pays ?',
    //     answer: {
    //       country: 'Republic of Tajikistan',
    //       flag: 'https://flagcdn.com/w320/tj.png',
    //       capital: [Array]
    //     }
    //   }
    let liste = [
    ]
    let index = data.length
    for(let i = 0; i <= 2; i++) {
        let random = getRandomInt(index)
        let countryInfo = getCountryInfo(data[random])

        liste.push(countryInfo)
    }


    return liste

}

export const createQuestion = (countries) => {
    return {tipType:"flag",tip:"un drapeau",desiredInfo:"pays2",choices:["pays1","pays2","pays3","pays4"]}
}